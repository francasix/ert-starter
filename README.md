# Starter Express server rendering React frontend side in Typescript

## Start
`npm run start`

### Thanks
* [https://github.com/artemyarulin/ts-node-express-react-views-typescript](https://github.com/artemyarulin/ts-node-express-react-views-typescript)

* [https://itnext.io/building-restful-web-apis-with-node-js-express-mongodb-and-typescript-part-2-98c34e3513a2](https://itnext.io/building-restful-web-apis-with-node-js-express-mongodb-and-typescript-part-2-98c34e3513a2)