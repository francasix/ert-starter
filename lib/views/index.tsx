import FirstComponent from './src/FirstComponent';
import * as React from 'react';
import { PropsFromServer } from './src/interfaces/PropsFromServer';

class HelloMessage extends React.Component<PropsFromServer> {
  render() {
    return (
      <div>
        <h1>Rendered from server side</h1>
        {this.props.msg}
        <FirstComponent />
      </div>
    );
  }
}

module.exports = HelloMessage;
