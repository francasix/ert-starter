import * as React from 'react';

import UserInterface from './interfaces/UserInterface';

export default class UserComponent extends React.Component<UserInterface, {}> {
  constructor(props: UserInterface) {
    super(props);
  }
  render() {
    return (
      <div>
        <h2>User Component</h2>
        <ul>
          <li>Hello, {this.props.name}</li>
          <li>You are {this.props.age} years old,</li>
          <li>You live at: {this.props.address}</li>
          <li>you were born: {this.props.dob.toDateString()}</li>
        </ul>
      </div>
    );
  }
}
