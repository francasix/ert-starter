import * as React from 'react';
import UserComponent from './UserComponent';

export default class FirstComponent extends React.Component<{}> {
  render() {
    return (
      <div>
        <h2>First component</h2>
        <div>
          <UserComponent
            name="User001"
            age={33}
            address="Paris"
            dob={new Date()}
          />
        </div>
      </div>
    );
  }
}
